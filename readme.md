# ECORP 3

Nos super ingénieurs ont piraté grace à une technique ultra secrete des data chez nos concurents de GCORP.

> Lancez le piratage

```sh
chmod +x ./init.sh
./init.sh
```

> Utilisez les données récupérées et mettre dans une BDD:

1. La liste des clients (nom, prénom, date de naissance, adresse, telephone, email).
2. la liste des produits (id, nom, prix)
3. la liste des commandes avec id du client, l'id des produits et les quantités.
